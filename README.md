# gpib_app #

### Simple web application which reads from GPIB instrument ###

* This application runs Flask server and executes voltage measurement from Keithley 2002 multimeter.
* 0.5

### Setup ###

* Installation

1.  Get linux-gpib source code from http://linux-gpib.sourceforge.net
2.  Go to linux-gpib directory and run:

    
    ```
    ./configure
    make
    sudo make install
    ```


3.  Clone this repository: 
    
    
    ```
    git clone https://orzep@bitbucket.org/orzep/gpib_app.git 
    ```

4.   

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact