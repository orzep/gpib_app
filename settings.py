import yaml

config_path = 'config.yml'
config_yaml = bytes()
with open(config_path) as f:
	config_yaml = f.read()
conf = yaml.safe_load(config_yaml)

