"""
Copyright 2016 Marcin Orzepowski

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import os
from settings import conf
from flask import Flask, render_template, url_for
import Gpib
import re
app = Flask(__name__)
dvm = Gpib.Gpib(conf["gpib"]["board"], conf["multimeter"]["address"])



@app.route('/')
def main_page():
    dvm.write("*IDN?")
    meter_name = dvm.read(100).decode("utf8")
    return render_template('index.html', meter_name=meter_name)

@app.route('/dvm')
def read_meter():
   dvm.write(":FETCH?")
   resp = dvm.read(100).decode("utf8")
   #print(resp)
   resp = re.split(",", resp)
   return resp[0]

@app.route('/<path:path>')
def static_proxy(path):
  # send_static_file will guess the correct MIME type
  return app.send_static_file(path)


if __name__ == "__main__":
    app.run(host=conf["server"]["ip"], port=int(conf["server"]["port"]), debug=True)


